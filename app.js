const express = require("express");
const session = require("express-session");
const FileStore = require('session-file-store')(session);
const path = require("path");
const url = require("url");

const HOST_NAME = "127.0.0.1";
const PORT = 3000;

const app = express();

app.set("views", path.join(__dirname, "view"));
app.set("view engine", "ejs");

app.use(session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
    store: new FileStore({})
}));

// Log request
app.use((req, res, next) => {
    let reqUrl = url.parse(req.url);
    let path = reqUrl.pathname;

    let date = new Date();
    let time = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}.${date.getMilliseconds()}`;

    console.log(`${time} ${req.method} ${path}`);

    next();
});

// Static files
app.use("/public", express.static(path.join(__dirname, "public")));
app.use("/public/fontawesome", express.static(
    path.join(__dirname, "node_modules", "@fortawesome", "fontawesome-free")
));
app.use("/public/js-cookie", express.static(
    path.join(__dirname, "node_modules", "js-cookie", "src")
));

// Dynamic routes
app.use("/", require("./routes/index"));
app.use("/pizzas", require("./routes/pizzas"));
app.use("/other_items", require("./routes/other-items"));
app.use("/user", require("./routes/users"));
app.use("/cart", require("./routes/cart"));

app.listen(PORT, HOST_NAME, () => {
    console.log(`Server started: http://${HOST_NAME}:${PORT}`);
});
