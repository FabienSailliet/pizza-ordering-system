const Database = require("sqlite-async");
const path = require("path");

let db;
Database.open(path.join(__dirname, "pizza.db"), Database.OPEN_READWRITE)
    .then(openDb => {
        db = openDb;
    })
    .catch(err => {
        throw err;
    });

class Pizza {
    static async allByCustomer(customer) {
        const crustSizes = (await CrustSize.all()).sort(
            (size1, size2) => size1.size - size2.size
        );
        let pizzas = (customer) ?
            await db.all("SELECT * FROM PizzaView WHERE creator IS NULL or creator=?", customer.id) :
            await db.all("SELECT * FROM PizzaView WHERE creator is NULL");

        // Fill pizzas' ingredients
        await Promise.all(pizzas.map(async pizza => {
            pizza.ingredients = await PizzaIngredient.getByPizza(pizza.id);
            pizza.sizes = crustSizes.map(crustSize => {
                return {
                    size: crustSize.size,
                    price: pizza.base_price * crustSize.price_multiplier
                }
            });
        }));

        return pizzas;
    }

    static async getById(id) {
        return await db.get("SELECT * FROM PizzaView WHERE id=?", id);
    }

    static async getByIdAndSize(id, size) {
        let pizza = await db.get("SELECT * FROM PizzaView WHERE id=?", id);
        let crustSize = await CrustSize.getBySize(size);

        return {
            id: id,
            name: pizza.name,
            size: size,
            price: pizza.base_price * crustSize.price_multiplier,
            creator: pizza.creator
        };
    }

    static async insert(pizza) {
        let result = await db.run(
            "INSERT INTO Pizza(name, crust_flavor, creator) VALUES (?, ?, ?)",
            [pizza.name, pizza.crustFlavour, pizza.creator]
        );

        await Promise.all(pizza.ingredients.map(async ingredient => {
            db.run(
                "INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (?, ?)",
                [result.lastID, ingredient]
            )
        }));
    }
}

class PizzaIngredient {
    static async all() {
        return await db.all("SELECT * FROM PizzaIngredient");
    }

    static async getByPizza(id) {
        return await db.all(
            "SELECT * FROM PizzaIngredient JOIN Pizza_PizzaIngredient PPI on PizzaIngredient.name = PPI.pizza_ingredient WHERE pizza = ?",
            id
        );
    }

    static async getByName(name) {
        return await db.get("SELECT * FROM PizzaIngredient WHERE name=?", name);
    }
}

class NonPizzaItem {
    static async all() {
        return await db.all("SELECT * FROM NonPizzaItem");
    }

    static async getByName(name) {
        return await db.get("SELECT * FROM NonPizzaItem WHERE name=?", name);
    }
}

class CrustFlavor {
    static async all() {
        return await db.all("SELECT * FROM CrustFlavor");
    }

    static async getByName(name) {
        return await db.get("SELECT * FROM CrustFlavor WHERE name=?", name);
    }
}

class CrustSize {
    static async all() {
        return await db.all("SELECT * FROM CrustSize");
    }

    static async getBySize(size) {
        return await db.get("SELECT * FROM CrustSize WHERE size=?", size);
    }

    static async getMinPriceMultiplier() {
        let crustSizes = await this.all();
        return Math.min(...crustSizes.map(crustSize => crustSize.price_multiplier));
    }
}

class Customer {
    static async getByEmail(email) {
            return await db.get(
                "SELECT * FROM Customer WHERE email=?",
                [email]
            );
    }

    static async insert(customer) {
        await db.run(
            "INSERT INTO Customer(email, first_name, last_name, password, phone_nb) VALUES (?, ?, ?, ?, ?)",
            [customer.email, customer.firstName, customer.lastName, customer.password, customer.phoneNumber]
        );
    }
}

class Order {
    static async insert(order, cart) {
        let result = await db.run(
            'INSERT INTO "Order"(delivery_address, paid, delivered, confirmation_datetime, card_nb, card_exp_date, customer) VALUES (?, ?, ?, ?, ?, ?, ?)',
            [
                (order.orderType === "delivery") ?  order.deliveryAddress : null,
                true,
                false,
                new Date().toISOString(),
                order.cardNb,
                order.cardExpDate,
                order.customer
            ]
        );

        let orderId = result.lastID;

        for (let pizzaId of Object.keys(cart.pizzas)) {
            for (let size of Object.keys(cart.pizzas[pizzaId])) {
                await db.run(
                    'INSERT INTO Order_Pizza(pizza, "order", crust_size, quantity) VALUES (?, ?, ?, ?)',
                    [pizzaId, orderId, size, cart.pizzas[pizzaId][size]]
                );
            }
        }
        for (let itemName of Object.keys(cart.otherItems)) {
            await db.run(
                'INSERT INTO Order_NonPizzaItems("order", non_pizza_item, quantity) VALUES (?, ?, ?)',
                [orderId, itemName, cart.otherItems[itemName]]
            );
        }
    }
}

module.exports = {
    Pizza: Pizza,
    PizzaIngredient: PizzaIngredient,
    NonPizzaItem: NonPizzaItem,
    CrustFlavor: CrustFlavor,
    CrustSize: CrustSize,
    Customer: Customer,
    Order: Order
};
