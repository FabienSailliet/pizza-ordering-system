DROP VIEW IF EXISTS PizzaView;
DROP TABLE IF EXISTS Customer;
DROP TABLE IF EXISTS NonPizzaItem;
DROP TABLE IF EXISTS PizzaIngredient;
DROP TABLE IF EXISTS CrustSize;
DROP TABLE IF EXISTS CrustFlavor;
DROP TABLE IF EXISTS Pizza;
DROP TABLE IF EXISTS "Order";
DROP TABLE IF EXISTS Order_Pizza;
DROP TABLE IF EXISTS Pizza_PizzaIngredient;
DROP TABLE IF EXISTS Order_NonPizzaItems;
CREATE TABLE Customer (
  id         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  email      text NOT NULL UNIQUE,
  first_name text NOT NULL,
  last_name  text NOT NULL,
  password   text NOT NULL,
  phone_nb   text);
CREATE TABLE NonPizzaItem (
  name  text NOT NULL,
  price real(10) NOT NULL,
  image blob DEFAULT 'NULL',
  PRIMARY KEY (name));
CREATE TABLE PizzaIngredient (
  name       text NOT NULL,
  base_price real(10) NOT NULL,
  vegetarian boolean NOT NULL,
  PRIMARY KEY (name));
CREATE TABLE CrustSize (
  "size"           real(10) NOT NULL,
  price_multiplier real(10) NOT NULL,
  PRIMARY KEY ("size"));
CREATE TABLE CrustFlavor (
  name       text NOT NULL,
  base_price real(10) NOT NULL,
  PRIMARY KEY (name));
CREATE TABLE Pizza (
  id           INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name         text NOT NULL,
  image_url    varchar(255) DEFAULT NULL,
  crust_flavor text NOT NULL,
  creator      integer(10) DEFAULT NULL,
  FOREIGN KEY(crust_flavor) REFERENCES CrustFlavor(name),
  FOREIGN KEY(creator) REFERENCES Customer(id));
CREATE TABLE "Order" (
  id                    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  delivery_address      text,
  paid                  boolean NOT NULL,
  delivered             boolean NOT NULL,
  confirmation_datetime integer(10),
  card_nb               varchar(255) NOT NULL,
  card_exp_date         date NOT NULL,
  customer              integer(10) NOT NULL,
  FOREIGN KEY(customer) REFERENCES Customer(id));
CREATE TABLE Order_Pizza (
  pizza      integer(10) NOT NULL,
  "order"    integer(10) NOT NULL,
  crust_size real(10) NOT NULL,
  quantity   integer(10) NOT NULL,
  PRIMARY KEY (pizza,
  "order",
  crust_size),
  FOREIGN KEY(pizza) REFERENCES Pizza(id),
  FOREIGN KEY("order") REFERENCES "Order"(id),
  FOREIGN KEY(crust_size) REFERENCES CrustSize("size"));
CREATE TABLE Pizza_PizzaIngredient (
  pizza            integer(10) NOT NULL,
  pizza_ingredient text NOT NULL,
  PRIMARY KEY (pizza,
  pizza_ingredient),
  FOREIGN KEY(pizza) REFERENCES Pizza(id),
  FOREIGN KEY(pizza_ingredient) REFERENCES PizzaIngredient(name));
CREATE TABLE Order_NonPizzaItems (
  "order"        integer(10) NOT NULL,
  non_pizza_item text NOT NULL,
  quantity       integer(10) NOT NULL,
  PRIMARY KEY ("order",
  non_pizza_item),
  FOREIGN KEY("order") REFERENCES "Order"(id),
  FOREIGN KEY(non_pizza_item) REFERENCES NonPizzaItem(name));
CREATE VIEW PizzaView AS
SELECT
    id,
    Pizza.name,
    crust_flavor,
    image_url,
    creator,
    SUM(base_price) + (SELECT base_price FROM CrustFlavor WHERE CrustFlavor.name = Pizza.crust_flavor) base_price,
    case when SUM(vegetarian) = COUNT(*) then true else false end vegetarian
FROM Pizza
    JOIN Pizza_PizzaIngredient PPI on Pizza.id = PPI.pizza
    JOIN PizzaIngredient PI on PPI.pizza_ingredient = PI.name
GROUP BY id;
INSERT INTO Customer(id, email, first_name, last_name, password, phone_nb) VALUES (1, 'fabien.sailliet@mycit.ie', 'Fabien', 'Sailliet', 'password', null);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Chips', 2, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Coke Can', 1, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Fanta Can', 1, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Sprite Can', 1, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Cookie', 1.5, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Chocolate Ice Cream', 4, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Caramel Ice Cream', 4, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Cookie Ice Cream', 4, NULL);
INSERT INTO NonPizzaItem(name, price, image) VALUES ('Vanilla Ice Cream', 4, NULL);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Pepperoni', 1, false);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Onions', 0.5, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Bacon', 1, false);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Ham', 1, false);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Mushrooms', 1, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Green Peppers', 0.5, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Red Peppers', 0.5, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Sweetcorn', 1, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Mozzarella', 1, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Parmesan', 1, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Brie', 1, true);
INSERT INTO PizzaIngredient(name, base_price, vegetarian) VALUES ('Ricotta', 1, true);
INSERT INTO CrustSize("size", price_multiplier) VALUES (30, 1);
INSERT INTO CrustSize("size", price_multiplier) VALUES (20, 0.75);
INSERT INTO CrustSize("size", price_multiplier) VALUES (40, 1.5);
INSERT INTO CrustFlavor(name, base_price) VALUES ('Normal', 1);
INSERT INTO CrustFlavor(name, base_price) VALUES ('Cheesy', 2);
INSERT INTO Pizza(id, name, image_url, crust_flavor, creator) VALUES (1, 'The Empire State', '/public/img/pizzas/empire-state.png', 'Normal', null);
INSERT INTO Pizza(id, name, image_url, crust_flavor, creator) VALUES (2, '4 Cheese', '/public/img/pizzas/4-cheese.png', 'Normal', null);
INSERT INTO Pizza(id, name, image_url, crust_flavor, creator) VALUES (3, 'Cheesy', '/public/img/pizzas/cheesy.png', 'Cheesy', null);
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (1, 'Pepperoni');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (1, 'Onions');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (1, 'Ham');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (1, 'Mushrooms');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (1, 'Green Peppers');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (1, 'Sweetcorn');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (2, 'Mozzarella');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (2, 'Parmesan');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (2, 'Brie');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (2, 'Ricotta');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (3, 'Mozzarella');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (3, 'Parmesan');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (3, 'Brie');
INSERT INTO Pizza_PizzaIngredient(pizza, pizza_ingredient) VALUES (3, 'Ricotta');
