#!/usr/bin/env bash

cd "$(dirname "$0")" || exit

sqlite3 pizza.db < pizza.sql
echo "Generated database"