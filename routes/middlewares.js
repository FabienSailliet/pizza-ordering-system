const cookieParser = require("cookie-parser");

module.exports.requireLogin = (req, res, next) => {
    if (req.session.customer) {
        next();
    }
    else {
        res.redirect("/user/log-in");
    }
};

module.exports.parseCart = (req, res, next) => {
    cookieParser()(req, res, () => {
        let cart;
        if (req.cookies.basket) {
            try {
                cart = JSON.parse(req.cookies.basket);
            }
            catch (e) {
                cart = {};
            }
        }
        else {
            cart = {}
        }

        if (!cart.pizzas) {
            cart.pizzas = {};
        }
        if (!cart.otherItems) {
            cart.otherItems = {};
        }
        req.cart = cart;

        next();
    });
};