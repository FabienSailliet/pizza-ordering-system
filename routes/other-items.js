const express = require("express");
const database = require("../model/database");

const router = express.Router();

router.get("/", async (req, res) => {
    res.render("pages/other-items", {
        items: await database.NonPizzaItem.all(),
        connectedUser: req.session.customer
    });
});

module.exports = router;
