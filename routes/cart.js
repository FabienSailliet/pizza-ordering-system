const express = require("express");
const database = require("../model/database");
const middlewares = require("./middlewares");

const router = express.Router();

router.get("/", middlewares.parseCart);
router.get("/", async (req, res) => {
    // Filter pizzas to keep only those who can be ordered by anyone or by the connected customer
    let allowedPizzas = {};
    for (let pizzaId of Object.keys(req.cart.pizzas)) {
        let pizza = await database.Pizza.getById(pizzaId);
        if (
            pizza.creator === null ||
            (req.session.customer !== undefined && pizza.creator === req.session.customer.id)
        ) {
            allowedPizzas[pizzaId] = req.cart.pizzas[pizzaId];
        }
    }
    req.cart.pizzas = allowedPizzas;

    let items = {pizzas: [], otherItems: []};
    for (let pizzaId of Object.keys(req.cart.pizzas)) {
        for (let size of Object.keys(req.cart.pizzas[pizzaId])) {
            let pizza = await database.Pizza.getByIdAndSize(pizzaId, size);
            pizza.quantity = req.cart.pizzas[pizzaId][size];
            items.pizzas.push(pizza);
        }
    }
    for (let itemName of Object.keys(req.cart.otherItems)) {
        let item = await database.NonPizzaItem.getByName(itemName);
        item.quantity = req.cart.otherItems[itemName];
        items.otherItems.push(item);
    }

    res.cookie("basket", JSON.stringify(req.cart));

    res.render("pages/cart", {
        connectedUser: req.session.customer,
        items: items
    });
});

router.get("/price", middlewares.parseCart);
router.get("/price", async (req, res) => {
    res.send(JSON.stringify(await getCartPrice(req.cart)));
});

router.use("/confirm", middlewares.requireLogin);
router.use("/confirm", middlewares.parseCart);

router.get("/confirm", async (req, res) => {
    res.render("pages/cart-confirmation", {
        connectedUser: req.session.customer,
        price: await getCartPrice(req.cart)
    });
});

router.post("/confirm", express.urlencoded({extended: true}));
router.post("/confirm", async (req, res) => {
    let order = req.body;
    order.customer = req.session.customer.id;
    await database.Order.insert(order, req.cart);

    let readyMessage = (order.orderType === "delivery") ? "it will be delivered" : "you can get it in the shop";

    res.cookie("basket", JSON.stringify({}));

    res.render("pages/order-confirmation.ejs", {
        connectedUser: req.session.customer,
        message: `The ordered has been registered, ${readyMessage} in one hour`
    });
});

async function getCartPrice(cart) {
    let price = 0;

    for (let pizzaId of Object.keys(cart.pizzas)) {
        for (let size of Object.keys(cart.pizzas[pizzaId])) {
            let pizza = await database.Pizza.getByIdAndSize(pizzaId, size);
            let quantity = cart.pizzas[pizzaId][size];

            price += pizza.price * quantity;
        }
    }
    for (let itemName of Object.keys(cart.otherItems)) {
        let item = await database.NonPizzaItem.getByName(itemName);
        let quantity = cart.otherItems[itemName];

        price += item.price * quantity;
    }

    return price;
}

module.exports = router;
