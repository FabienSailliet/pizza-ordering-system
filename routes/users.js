const express = require("express");
const database = require("../model/database");

const router = express.Router();

router.get("/log-in", async (req, res) => {
    if (req.session.customer) {
        res.redirect("/");
    }
    else {
        res.render("pages/log-in", {
            connectedUser: req.session.customer
        });
    }
});

router.post("/log-in", express.urlencoded({extended: true}));
router.post("/log-in", async (req, res, next) => {
    let customer = await database.Customer.getByEmail(req.body.email);

    try {
        if (customer) {
            if (req.body.password === customer.password) {
                req.session.customer = customer;
                res.redirect("/pizzas");
            }
            else {
                throw new Error("Invalid password");
            }
        } else {
            throw new Error(`Cannot find customer with email ${req.body.email}`);
        }
    }
    catch (e) {
        res.render(
            "pages/log-in",
            {
                errorMessage: e.message,
                connectedUser: req.session.customer
            }
        );
    }
});

router.get("/sign-up", async (req, res) => {
    if (req.session.customer) {
        res.redirect("/pizzas");
    }
    else {
        res.render("pages/sign-up", {
            connectedUser: req.session.customer
        });
    }
});

router.post("/sign-up", express.urlencoded({extended: true}));
router.post("/sign-up", async (req, res, next) => {
    let customer = req.body;

    if (typeof customer.firstName === "undefined") {
        next(new Error("First name is missing"));
    }
    else if (typeof customer.lastName === "undefined") {
        next(new Error("Last name is missing"));
    }
    else if (typeof customer.phoneNumber === "undefined") {
        next(new Error("Phone number is missing"));
    }
    else if (typeof customer.email === "undefined") {
        next(new Error("Email is missing"));
    }
    else if (typeof customer.password === "undefined") {
        next(new Error("Password is missing"));
    }
    else {
        try {
            if (await database.Customer.getByEmail(customer.email)) {
                throw new Error(`The email address ${customer.email} is already taken`);
            }

            await database.Customer.insert(customer);
            let addedCustomer = await database.Customer.getByEmail(customer.email);
            req.session.customer = addedCustomer;
            res.redirect("/pizzas")
        }
        catch (e) {
            res.render(
                "pages/sign-up",
                {
                    errorMessage: e.message,
                    connectedUser: req.session.customer
                }
            );
        }
    }
});

router.get("/log-out", async (req, res) => {
    delete req.session.customer;
    res.redirect("/");
});

module.exports = router;
