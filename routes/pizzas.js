const express = require("express");
const database = require("../model/database");
const middlewares = require("./middlewares");

const router = express.Router();

router.get("/", async (req, res) => {
    let pizzas = await database.Pizza.allByCustomer(req.session.customer);

    res.render("pages/pizzas", {
        pizzas: pizzas,
        connectedUser: req.session.customer
    });
});

router.use("/make_your_own", middlewares.requireLogin);

// Pizza creation page
router.get("/make_your_own", async (req, res) => {
    let minPriceMultiplier = await database.CrustSize.getMinPriceMultiplier();
    let ingredients = await database.PizzaIngredient.all();
    for (let ingredient of ingredients) {
        ingredient.minPrice = ingredient.base_price * minPriceMultiplier;
    }

    res.render("pages/create-pizza", {
        ingredients: ingredients,
        crustFlavours: await database.CrustFlavor.all(),
        connectedUser: req.session.customer
    });
});

// Pizza creation POST endpoint, create and redirect to pizzas list
router.post("/make_your_own", express.urlencoded({extended: true}));
router.post("/make_your_own", async (req, res, next) => {
    let pizza = req.body;

    if (typeof pizza.ingredients === "undefined") {
        next(new Error("Cannot create a pizza without ingredient"));
    }
    else if (typeof pizza.name === "undefined") {
        next(new Error("Cannot create a pizza without a name"));
    }
    else if (typeof pizza.crustFlavour === "undefined") {
        next(new Error("Cannot create a pizza without a crust flavour"));
    }
    else {
        pizza.creator = req.session.customer.id;
        await database.Pizza.insert(pizza);
        res.redirect("/pizzas")
    }
});

router.get("/price", async (req, res) => {
    let minPriceMultiplier = await database.CrustSize.getMinPriceMultiplier();
    let minPrice = 0;

    if (req.query.crustFlavour) {
        let crustFlavour = await database.CrustFlavor.getByName(req.query.crustFlavour);
        if (crustFlavour) {
            minPrice += crustFlavour.base_price * minPriceMultiplier;
        }
    }
    if (req.query.ingredients) {
        for (let ingredientName of req.query.ingredients) {
            let ingredient = await database.PizzaIngredient.getByName(ingredientName);
            if (ingredient) {
                minPrice += ingredient.base_price * minPriceMultiplier;
            }
        }
    }

    res.send(JSON.stringify(minPrice));
});

module.exports = router;
