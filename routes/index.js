const express = require("express");

const router = express.Router();

router.get("/", (req, res) => {
    res.render("index", {
        connectedUser: req.session.customer
    });
});

module.exports = router;
