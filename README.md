# Pizza Ordering System

## Installation

Just run in the root directory:

```
npm install
```

This will install the necessary dependencies and then generate the SQLite database in the `model` folder with a Bash script. If this step doesn't work, generate the database from the SQL script `model/pizza.sql` into `model/pizza.db`.

## Run

To run the website, just run:

```
npm start
```

This should start the webserver in localhost on port 3000 and print a link to access it.

The host and port can both be changed on the top of the `app.js` file, with the two following lines:

```javascript
const HOST_NAME = "127.0.0.1";
const PORT = 3000;
```

## Demonstration Video

![screen-cast](readme/screen-cast.mp4)

## Images

### Sign up page

![sign up page](readme/sign_up.png)

### Pizzas List

![pizzas page](readme/pizzas.png)

### Other Items List

![other items page](readme/other_items.png)

### Pizza Creation

![pizza creation page](readme/create_pizza.png)

### Cart

![cart page](readme/cart.png)
