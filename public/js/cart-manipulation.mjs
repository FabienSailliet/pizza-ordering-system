export function updateCart(callback) {
    let basketStr = Cookies.get("basket");

    let basket = (basketStr) ?
        JSON.parse(basketStr) :
        {};

    if (basket.pizzas === undefined) {
        basket.pizzas = {};
    }

    if (basket.otherItems === undefined) {
        basket.otherItems = {};
    }

    callback(basket);

    Cookies.set("basket", JSON.stringify(basket));
}