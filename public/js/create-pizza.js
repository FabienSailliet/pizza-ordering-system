window.onload = function() {
    let form = document.getElementById("create-pizza-form");

    for (let checkbox of form["ingredients[]"]) {
        checkbox.addEventListener("change", updateIngredientsCheckboxesValidity);
        checkbox.addEventListener("change", updatePrice);
    }

    form.crustFlavour.onchange = updatePrice;

    updateIngredientsCheckboxesValidity();
    updatePrice().then();
};

function updateIngredientsCheckboxesValidity() {
    let form = document.getElementById("create-pizza-form");

    // If no selected ingredients, indicate first checkbox as invalid
    if (!form.querySelector("input[type=checkbox]:checked")) {
        form["ingredients[]"][0].setCustomValidity("You need to select at least one ingredient.");
    }
    else {
        form["ingredients[]"][0].setCustomValidity("");
    }
}

async function updatePrice() {
    let formData = new FormData(document.getElementById("create-pizza-form"));
    let query = [];
    query.push(...formData.getAll("ingredients[]")
        .map(ingredient => `ingredients[]=${encodeURIComponent(ingredient)}`));
    query.push(`crustFlavour=${formData.get("crustFlavour")}`);

    let minPrice = await (await fetch(`/pizzas/price?${query.join("&")}`)).json();

    document.getElementById("pizza-min-price").textContent = minPrice.toFixed(2) + "€";
}
