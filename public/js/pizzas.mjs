import {updateCart} from "/public/js/cart-manipulation.mjs";

window.onload = () => {
    let pizzasList = document.getElementById("pizzas-list");

    // Loop through displayed pizzas
    for (let pizza of pizzasList.querySelectorAll(".pizza")) {
        // Expand or retract available sizes
        pizza.querySelector(".content").onclick = () => {
            switchPizzaExpandable(pizza)
        };

        // Manage pizzas' size buttons
        for (let sizeRow of pizza.querySelectorAll(".size-row")) {
            let orderButton = sizeRow.querySelector(".price");
            orderButton.onclick = () => {
                addToCart(pizza.dataset.id, sizeRow.dataset.size);
            }
        }

        updateExpandIcon(pizza);
    }
};

function switchPizzaExpandable(pizzaRow) {
    let expandable = pizzaRow.querySelector(".expandable");
    expandable.hidden = !expandable.hidden;
    updateExpandIcon(pizzaRow)
}

function updateExpandIcon(pizzaRow) {
    pizzaRow.querySelector(".develop-div i").className =
        (pizzaRow.querySelector(".expandable").hidden) ?
            "fas fa-caret-right" :
            "fas fa-caret-down";
}

function addToCart(pizzaId, crustSize) {
    updateCart((cart) => {
        if (cart.pizzas[pizzaId] === undefined) {
            cart.pizzas[pizzaId] = {};
        }

        if (cart.pizzas[pizzaId][crustSize] === undefined) {
            cart.pizzas[pizzaId][crustSize] = 0;
        }

        cart.pizzas[pizzaId][crustSize]++;
    });

    info("The pizza has been added to the cart");
}