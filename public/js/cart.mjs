import {updateCart} from "/public/js/cart-manipulation.mjs";

window.onload = async () => {
    await updateTotalPrice();

    for (let row of document.getElementById("pizzas").querySelectorAll("tbody tr")) {
        // Manage delete button click
        row.querySelector(".delete i").onclick = async () => {
            // Remove line from DOM
            row.remove();

            // Remove pizza from cookie
            updateCart((cart) => {
                // Remove size from cookie
                delete cart.pizzas[row.dataset.pizzaId][row.dataset.pizzaSize];

                // If deleted last size, delete pizza from cart
                if (Object.keys(cart.pizzas[row.dataset.pizzaId]).length === 0) {
                    delete cart.pizzas[row.dataset.pizzaId];
                }
            });

            await updateTotalPrice();
        };

        let quantityInput = row.querySelector(".quantity input");

        // Update pizza quantity
        quantityInput.onchange = async () => {
            updateCart((cart) => {
                cart.pizzas[row.dataset.pizzaId][row.dataset.pizzaSize] = quantityInput.value;
            });

            await updateTotalPrice();
        }
    }

    for (let row of document.getElementById("other-items").querySelectorAll("tbody tr")) {
        // Manage delete button click
        row.querySelector(".delete i").onclick = async () => {
            // Remove line from DOM
            row.remove();

            // Remove item from cookie
            updateCart((cart) => {
                // Remove item from cookie
                delete cart.otherItems[row.dataset.itemName];
            });

            await updateTotalPrice();
        };

        let quantityInput = row.querySelector(".quantity input");

        // Update pizza quantity
        quantityInput.onchange = async () => {
            updateCart((cart) => {
                cart.otherItems[row.dataset.itemName] = quantityInput.value;
            });

            await updateTotalPrice();
        }
    }
};

async function updateTotalPrice() {
    let price = await (await fetch("/cart/price")).json();
    document.getElementById("total-price").textContent = price.toFixed(2) + "€";
    document.getElementById("confirm-button-section").hidden = (price === 0);
}