import {updateCart} from "/public/js/cart-manipulation.mjs";

window.onload = () => {
    let itemsList = document.getElementById("other-items-list");

    // Loop through displayed items
    for (let item of itemsList.querySelectorAll(".item")) {
        // Manage items' click
        item.onclick = () => {
            addToCart(item.dataset.name);
        };
    }
};

function addToCart(itemName) {
    updateCart((cart) => {
        if (cart.otherItems[itemName] === undefined) {
            cart.otherItems[itemName] = 0;
        }

        cart.otherItems[itemName]++;
    });

    info("The item has been added to the cart");
}