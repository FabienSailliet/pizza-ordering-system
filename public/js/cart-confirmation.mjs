window.onload = () => {
    let form = document.getElementById("cart-confirmation-form");

    for (let radio of document.querySelectorAll("#order-type-section input[type=radio]")) {
        radio.onchange = () => {
            document.getElementById("delivery-address-section").hidden =
                (form.orderType.value !== "delivery");
        };
    }
};